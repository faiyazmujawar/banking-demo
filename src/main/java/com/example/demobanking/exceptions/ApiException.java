package com.example.demobanking.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@RequiredArgsConstructor
@AllArgsConstructor
public class ApiException extends RuntimeException {
    private final HttpStatus httpStatus;
    private final String message;
    private Collection<String> errors = new ArrayList<>();
}
