package com.example.demobanking.exceptions;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static org.springframework.http.HttpStatus.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionUtils {
    public static ApiException notFound(String message) {
        return new ApiException(NOT_FOUND, message);
    }

    public static ApiException badRequest(String message) {
        return new ApiException(BAD_REQUEST, message);
    }

    public static ApiException forbidden(String message) {
        return new ApiException(FORBIDDEN, message);
    }
}
