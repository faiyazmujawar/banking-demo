package com.example.demobanking.services;

import com.example.demobanking.UserMapper;
import com.example.demobanking.api.request.LoginRequest;
import com.example.demobanking.api.request.SignupRequest;
import com.example.demobanking.api.response.AuthResponse;
import com.example.demobanking.entities.User;
import com.example.demobanking.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.example.demobanking.exceptions.ExceptionUtils.badRequest;
import static com.example.demobanking.exceptions.ExceptionUtils.forbidden;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    @Value("${jwt.token_expiration}")
    private Long JWT_EXPIRATION;

    public AuthResponse signup(SignupRequest request) {
        userRepository.findByEmailOrUsername(request.getEmail(), request.getUsername())
                .ifPresent(user -> {
                    if (user.getEmail().equals(request.getEmail())) {
                        throw badRequest("Email {%s} already in use".formatted(request.getEmail()));
                    }
                    throw badRequest("Username {%s} already in use".formatted(request.getUsername()));
                });
        var user = userMapper.toUser(request);
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        return authResponse(userRepository.save(user));
    }

    private AuthResponse authResponse(User user) {
        Map<String, Object> payload = Map.of("uid", user.getId());
        var token = jwtService.generateToken(user.getUsername(), payload, JWT_EXPIRATION);
        var refreshToken = jwtService.generateToken(user.getUsername(), payload, JWT_EXPIRATION * 10);
        return new AuthResponse(token, refreshToken);
    }

    public AuthResponse login(LoginRequest request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            var user = userRepository.findByUsername(request.getUsername()).get();
            return authResponse(user);
        } catch (AuthenticationException e) {
            throw forbidden("Bad Credentials");
        }
    }
}
