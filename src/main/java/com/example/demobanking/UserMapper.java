package com.example.demobanking;

import com.example.demobanking.api.request.SignupRequest;
import com.example.demobanking.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    User toUser(SignupRequest request);
}
