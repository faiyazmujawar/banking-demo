package com.example.demobanking.api.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class SignupRequest {
    private String firstname;
    private String lastname;
    private String username;
    @Email
    private String email;
    @NotBlank
    @Pattern(regexp = ".{6,}")
    private String password;
    private Integer contact;
}
