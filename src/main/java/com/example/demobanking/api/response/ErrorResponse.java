package com.example.demobanking.api.response;

import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Data
public class ErrorResponse {
    private String message;
    private Instant timestamp = Instant.now();
    private Collection<String> errors = new ArrayList<>();

    public ErrorResponse(String message) {
        this.message = message;
    }

    public ErrorResponse(String message, Collection<String> errors) {
        this.message = message;
        this.errors = errors;
    }
}
