package com.example.demobanking.api.controllers;

import com.example.demobanking.api.request.LoginRequest;
import com.example.demobanking.api.request.SignupRequest;
import com.example.demobanking.api.response.AuthResponse;
import com.example.demobanking.services.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth")
@RequiredArgsConstructor
public class Auth {
    private final AuthService authService;

    @PostMapping(path = "/register")
    public AuthResponse signup(@RequestBody @Valid SignupRequest request) {
        return authService.signup(request);
    }

    @PostMapping(path = "/login")
    public AuthResponse authResponse(@RequestBody @Valid LoginRequest request) {
        return authService.login(request);
    }
}
